# Artichoke-BackEnd

A Hotel Application created by Team 4

## Requirments

For building and running the application you need:
- [Docker](https://www.docker.com/)

## Commands to run this application in dev environement
- Put the MySQL root password in `.mysql_db_pass` file (without .extension) next to the `docker-compose.yml` file.
```bash
echo choose_a_mysql_password > .mysql_db_pass
```
- MySQL port is `6603`.

- Run command at the root folder to start docker service 
```bash
docker compose --profile dev up --build -d
```

## Commands to run this application in staging environement
- Put the MySQL root password in `.mysql_db_pass` file (without .extension) next to the `docker-compose.yml` file.
```bash
echo choose_a_mysql_password > .mysql_db_pass
```

- MySQL port is default `3306`, but it can't be accessed from outside docker thanks to docker networks.
```bash
docker compose --profile staging up --build -d
```

## Documentation
All document can be found in the Drive at : [Google Drive](https://drive.google.com/drive/folders/1JiEBcvAJXo2nodaaEeolWCMAbvG9h85l)

Shortscut:
- [New commer](https://docs.google.com/document/d/1WKUZM0Y63VrkfaDYU5erx-_JL7OiNpveIF72OlPRrUo/edit?usp=drive_link)
- [Git Strategy](https://drive.google.com/file/d/1hWdzB44RAbr2M3cYQoe7S8VMsHpAIoZO/view?usp=drive_link)
- [API specification](https://docs.google.com/spreadsheets/d/1N_5i6dn276t_2lkTienhye9QbGj1TgKOqeEP6bepoZA/edit?usp=drive_link)
- [MLD](https://drive.google.com/file/d/1_b2LidDzm0mSzVkUOwkaFppEVRI8vJtS/view?usp=drive_link)
- [Deployment diagram](https://drive.google.com/file/d/1NyHca4ZnHx6j3me3mMuH4ODGGabYIB07/view?usp=drive_link)

## Git Strategy

**main** : Is the production branch. All modification are automatically deployed on the prod server. **Be carefull**.
**staging** : Is the staging branch. All modification are automatically deployed on the staging server. **Be carefull**.
**dev** : Is the *default* branch. **Good choice to start coding**. Create branch from dev, code, and make a merge request. All commit on dev is submit to the linter 😈 (currently not setup)

## TODO
- [x] configure CI/CD staging environement
- [ ] mysql user. Delete root, create one with required privilege only.

test deployment