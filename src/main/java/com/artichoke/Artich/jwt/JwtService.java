package com.artichoke.Artich.jwt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class JwtService {
	public String GetJwtToken(String login) {
		var tokenExp = Calendar.getInstance();
		//tokenExp.add(Calendar.DAY_OF_MONTH, 1);
		tokenExp.add(Calendar.SECOND, 1);

		/*var refreshExp = Calendar.getInstance();
		refreshExp.add(Calendar.WEEK_OF_MONTH, 1);*/

		String jwtToken = Jwts.builder()
				.claim("name", login)
				//.claim("authorities", TROLOLOL)
				.subject(login)
				//.setId(UUID.randomUUID().toString())
				//.setIssuedAt(Date.from(now))
				.expiration(tokenExp.getTime())
				.signWith(Keys.hmacShaKeyFor("ruhqsufgquigqiugiuhfgruhqsufgquigqiugiuhfgruhqsufgquigqiugiuhfgruhqsufgquigqiugiuhfg".getBytes()), Jwts.SIG.HS512)
				.header().add("alg", Jwts.SIG.HS512.getId()).and()
				.compact();

		/*String jwtRefresh = Jwts.builder()
				.subject(authenticationParams.username)
				.expiration(refreshExp.getTime())
				.compact();*/

		return jwtToken;
	}
}
