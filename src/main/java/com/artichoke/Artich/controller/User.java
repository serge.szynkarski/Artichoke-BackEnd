package com.artichoke.Artich.controller;

import com.artichoke.Artich.entity.Account;
import com.artichoke.Artich.jwt.JwtService;
import com.artichoke.Artich.repository.AccountRepository;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/User")
public class User {
	@Autowired
	private JwtService jwtService;

	@Autowired
	private AccountRepository accountRepository;

	@PostMapping()
	public String CreateUser(@RequestBody UserRegistration userRegistration) {
		accountRepository.save(new Account(userRegistration.login, userRegistration.password));
		return "Create user"+jwtService.GetJwtToken(userRegistration.login);
	}

	@GetMapping()
	public String GetUsers() {
		return "Get users";
	}

	public record UserRegistration (@Pattern(regexp = "[a-z0-9A-Z]+") String login, @NotBlank String password) {

	}
}
