package com.artichoke.Artich;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtichApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtichApplication.class, args);
	}

}
