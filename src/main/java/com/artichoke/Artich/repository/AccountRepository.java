package com.artichoke.Artich.repository;

import com.artichoke.Artich.entity.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {
	Account findByLogin(String Login_account);
}
