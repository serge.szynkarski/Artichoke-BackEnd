package com.artichoke.Artich.entity;

import jakarta.persistence.*;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Arrays;
import java.util.Collection;

@Entity
@Table(name="account")
public class Account implements UserDetails {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id_account;

	@Column(name="login_account")
	private String login;

	@Column(name="password_account")
	private String password;

	@Column(name="authorities_account")
	private String authorities;

	public Account(String login, String password) {
		this.login=login;
		this.password=password;
		this.authorities = "ROLE_USER";
	}

	public Account() {

	}

	public long getId() {
		return id_account;
	}

	public void setId(Long id) {
		this.id_account = id;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Arrays.stream(authorities.split(";")).map(SimpleGrantedAuthority::new).toList();
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return login;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
