# Use the official OpenJDK base image
FROM openjdk:21-slim-bullseye

# Set the working directory inside the container
WORKDIR /app

# Copy the compiled Java application JAR file into the container at /app
COPY target/Artich-0.0.1-SNAPSHOT.jar /app/Artich-0.0.1-SNAPSHOT.jar

# Expose port 8080
EXPOSE 8080

# Command to run your Java application
CMD ["java", "-jar", "-Dspring.profiles.active=prod", "Artich-0.0.1-SNAPSHOT.jar"]